/*
    const MongoClient = require('mongodb').MongoClient;
 const test = require('assert');
 // Connection url
 const url = 'mongodb://localhost:27017';
 // Database Name
 const dbName = 'test';


 // Connect using MongoClient
 MongoClient.connect(url, function(err, client) {
   // Use the admin database for the operation
   const adminDb = client.db(dbName).admin();


   // List all the available databases
   adminDb.listDatabases(function(err, dbs) {
     test.equal(null, err);
     test.ok(dbs.databases.length > 0);
     client.close();
   });
   });
    */
open Relude.Globals;

type t;

[@bs.module "mongodb"]
external connect:
  (
    string, // connection URL
    (Js.Null_undefined.t(Js.Exn.t), Js.Null_undefined.t(t)) => unit
  ) =>
  unit =
  "connect";

let connect = url =>
  IO.async(complete =>
    connect(
      url,
      (err, connection) => {
        let err = Js.Null_undefined.toOption(err);
        let connection = Js.Null_undefined.toOption(connection);

        let result =
          switch (err, connection) {
          | (Some(e), _) => Result.error(e)
          | (None, None) =>
            RJs.Exn.make("Connection was empty") |> Result.error
          | (None, Some(connection)) => Result.ok(connection)
          };

        complete(result);
      },
    )
  );

[@bs.send] external db: (t, string) => Db.t = "db";

let db = (t, url) =>
  try (db(t, url) |> Result.ok) {
  | Js.Exn.Error(e) => Result.error(e)
  };
