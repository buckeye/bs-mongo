type t;

[@bs.module "mongodb"] external make: string => t = "ObjectID";

external toJSON: t => Js.Json.t = "%identity";
